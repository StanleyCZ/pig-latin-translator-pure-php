<?php

class PigLatinTranslatorController extends BaseController
{
    protected PigLatinTranslatorModel $model;

    /**
     * PigLatinTranslatorController constructor.
     */
    public function __construct()
    {
        $this->model = new PigLatinTranslatorModel();
    }

    /**
     * Gets translation of the input text for the view.
     */
    public function translate()
    {
        $input = isset($_POST['input']) ? $_POST['input'] : '';
        $translation = $this->model->getPigTranslate($input);

        include 'views/translation.php';
    }

}