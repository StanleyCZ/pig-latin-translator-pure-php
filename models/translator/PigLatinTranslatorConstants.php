<?php

class PigLatinTranslatorConstants
{
    private const VOWELS = 'aeiou';
    private const CONSONANTS = 'bcdfghjklmnqrstvwxz';
    private const SPECIAL_CHARS = ',.!?';

    /**
     * Return vowels.
     *
     * @return string
     */
    public static function getVowels(): string
    {
        return self::VOWELS;
    }

    /**
     * Return consonants.
     *
     * @return string
     */
    public static function getConsonants(): string
    {
        return self::CONSONANTS;
    }

    /**
     * Return special characters.
     *
     * @return string
     */
    public static function getSpecialChars(): string
    {
        return self::SPECIAL_CHARS;
    }
}