<?php

class PigLatinTranslatorRulesModel
{
    /**
     * First letter (or letters) of the word vowel?
     *
     * @param string $word Word
     * @return bool true/false
     */
    public function checkVowelsFirst(string $word): bool
    {
        return preg_match('/^[' . PigLatinTranslatorConstants::getVowels() . ']/', $word) ? true : false;
    }

    /**
     * First letter (or letters) of the word consonant?
     *
     * @param string $word Word
     * @return bool true/false
     */
    public function checkConsonantsFirst(string $word): bool
    {
        return preg_match('/^[' . PigLatinTranslatorConstants::getConsonants() . 'y]/', $word) ? true : false;
    }

    /**
     * Last letter of the word special character?
     *
     * @param string $word Word
     * @return bool true/false
     */
    public function checkForSpecialCharacters(string $word): bool
    {
        return preg_match('/[' . PigLatinTranslatorConstants::getSpecialChars() . ']$/', $word) ? true : false;
    }
}