<?php

include_once 'controllers/BaseController.php';
include_once 'controllers/PigLatinTranslatorController.php';

include_once 'models/translator/PigLatinTranslatorModel.php';
include_once 'models/translator/PigLatinTranslatorRulesModel.php';
include_once 'models/translator/PigLatinTranslatorConstants.php';