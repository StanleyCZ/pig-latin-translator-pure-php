<?php

include_once 'autoloader.php';

/**
 * Process GET requests
 **/
$action = isset($_GET['action']) ? $_GET['action'] : 'showHomepage';
$controller = isset($_GET['controller']) ? $_GET['controller'] : '';

/**
 * Process POST requests, AJAX
 **/
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $action = isset($_GET['action']) ? $_GET['action'] : $action;
    $controller = isset($_GET['controller']) ? $_GET['controller'] : $controller;
}

switch ($controller) {
    case 'piglatintranslator':
        $controller = new PigLatinTranslatorController();
        break;
    default:
        $controller = new BaseController();
        break;
}

$controller->run($action);